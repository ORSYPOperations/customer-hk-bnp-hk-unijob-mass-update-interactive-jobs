package com.orsyp;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.orsyp.api.Context;
import com.orsyp.api.central.networknode.NetworkNodeId.PRODUCT_TYPE;
import com.orsyp.api.job.JobList;
import com.orsyp.central.jpa.jpo.NetworkNodeEntity;
import com.orsyp.stores.UVAPIWrapper;
import com.orsyp.util.Utils;

import edu.mscd.cs.jclo.JCLO;


class AllArgs {

	    private String node;
	    private String job;
	    private String interactive;
	    
}

public class Go {

	public static void main(String[] args) throws Exception{
		String Release = "1.0";
		String Customer = "BNP HK";
		
		System.out.println("=======================================================");
		System.out.println("  ** ORSYP Unijob Job Management Tool version " + Release +"  **  ");
		System.out.println("  *          ORSYP Professional Services.           * ");
		System.out.println("  * Copyright (c) 2014 ORSYP.  All rights reserved. * ");
		System.out.println("  **             Implemented for "+Customer+"            **");
		System.out.println("=======================================================");
		System.out.println("");
		
		JCLO jclo = new JCLO (new AllArgs());
        jclo.parse (args);
        String interactive=jclo.getString ("interactive");
        String JobFliter =jclo.getString ("job");
        String NodeFilter =jclo.getString ("node");
		
		System.out.println("=> Loading Program Options...");
		Properties pGlobalVariables = Utils.loadFile("/UJModule.properties",false);
		String login=pGlobalVariables.getProperty("UVMS_USER");
		String password=pGlobalVariables.getProperty("UVMS_PWD");   
		String uvmshostost=pGlobalVariables.getProperty("UVMS_HOST");   
		int port=Integer.parseInt(pGlobalVariables.getProperty("UVMS_PORT"));
		
		
			
		com.orsyp.stores.UVAPIWrapper conn = new com.orsyp.stores.UVAPIWrapper(
				uvmshostost, 
				port, 
				login, 
				password, 
				"UNIJOB", 
				"Exploitation"
				);		
		String nodenameFilter = NodeFilter; 
		String jobFilter = JobFliter;

		
		boolean isInteractive = false;
		if(interactive != null){
			if(interactive.equalsIgnoreCase("true") ||  interactive.equalsIgnoreCase("Y") || interactive.equalsIgnoreCase("yes")){
				isInteractive = true;
			}
		}
		
		// Filtering on UJ nodes first, and creating a list of nodes to process
		List<NetworkNodeEntity> networkNodeListFound = conn.UVMSStore.Nodes.getUVNodeList(PRODUCT_TYPE.UNIJOB);
		List<NetworkNodeEntity> networkNodeListToProcess = new ArrayList<NetworkNodeEntity>();
		
		for(int i=0;i<networkNodeListFound.size();i++){
			if(networkNodeListFound.get(i).getName().startsWith(nodenameFilter.replace("*",""))){
				networkNodeListToProcess.add(networkNodeListFound.get(i));
			}
		}
		
		if(networkNodeListToProcess.size()==0){System.out.println("%% No Unijob Node Matches Your Filter: "+nodenameFilter);}
		// For each node to process, we process all the jobs (if they match the filter of course).
		
		for(int j=0;j<networkNodeListToProcess.size();j++){
			NetworkNodeEntity ent = conn.UVMSStore.Nodes.getNodeEntityFromName(networkNodeListToProcess.get(j).getName(), PRODUCT_TYPE.UNIJOB);
			if(conn.UVMSStore.Nodes.isNodeAlive(ent, PRODUCT_TYPE.UNIJOB)){
				Context ct = conn.setCurrentNodeContext(ent);
				JobList listj = conn.UJ1Store.Jobs.getJobListWithFilter(jobFilter);
				if(listj.getCount()==0){System.out.println("%% Agent "+ent.getName()+": No Job Matches Your Filter: "+jobFilter);}
					for(int k=0;k<listj.getCount();k++){
						conn.UJ1Store.Jobs.setInteractiveBoxOnJob(conn.UJ1Store.Jobs.getJob(listj.get(k)),isInteractive);
						System.out.println("	++ Agent "+ent.getName()+": Setting Interactive Mode To: "+ isInteractive +" For Job :"+listj.get(k).getName());
					}
				}else{
					System.out.println("%% Node Unavailable: Skipping Node "+ent.getName());
				}
			}
		System.out.println("=> Done!");
		UVAPIWrapper.cleanup();
		System.exit(0);
		
		
	}
	
}
